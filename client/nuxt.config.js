require('dotenv').config();
const { path, join, resolve } = require('path');
const { copySync, removeSync } = require('fs-extra');
import fs from 'fs';

module.exports = {
	publicRuntimeConfig: {
		adg: process.env.ADG_URL || 'https://dolphins.test:3001',
		hr: process.env.HR_URL || 'https://hr.dolphins.test:3002',
		fdg: process.env.FDG_URL || 'https://fdg.dolphins.test:3003',
		pdg: process.env.PDG_URL || 'https://pdg.dolphins.test:3004',
		dms: process.env.DMS_URL || 'https://dms.dolphins.test:3007',
		jp: process.env.JP_URL || 'https://jp.dolphins.test:3006',
		enabled_domain: {
			dashboard: process.env.ADG_SITE,
			hr: process.env.HR_SITE,
			dms: process.env.DMS_SITE,
			pdg: process.env.PDG_SITE,
			fdg: process.env.FDG_SITE,
			jp: process.env.JP_SITE,
		},
		crr_name: process.env.CRR_NAME,
		production_domain: process.env.PRODUCTION_DOMAIN || 'dolphins.test',
	},
	server: {
		//change `<hotename>` with your generated domain by valet
		hostname: process.env.HOSTNAME || 'dolphins.test',
		port: process.env.APP_PORT, // default: 3000,
		https: {
			//change `<username>` with your machine username
			key: fs.readFileSync(resolve(process.env.KEY_PATH, process.env.HOSTNAME + '.key')),
			cert: fs.readFileSync(resolve(process.env.CERT_PATH, process.env.HOSTNAME + '.crt')),
		},
	},

	ssr: false,

	srcDir: __dirname,

	env: {
		apiUrl: process.env.API_URL || process.env.APP_URL + '/api',
		appName: process.env.APP_NAME || 'Dolphins - Administrative',
		appLocale: process.env.APP_LOCALE || 'en',
		githubAuth: !!process.env.GITHUB_CLIENT_ID,
		ADG_URL: process.env.ADG_URL,
		HR_URL: process.env.HR_URL,
		FDG_URL: process.env.FDG_URL,
		PDG_URL: process.env.PDG_URL,
		DMS_URL: process.env.DMS_URL,
		JP_URL: process.env.JP_URL,
	},

	head: {
		title: process.env.APP_NAME,
		titleTemplate: '%s | ' + process.env.APP_NAME,
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{
				hid: 'description',
				name: 'description',
				content: 'Maintain accessibility and availability of the entire system.',
			},
		],
		link: [
			{
				id: 'dolphinsFavicon',
				rel: 'icon',
				type: 'image/x-icon',
				href: '/favicon.png',
			},
		],
	},

	loading: { color: '#007bff' },

	router: {
		middleware: ['locale', 'check-auth'],
	},

	css: [{ src: '~assets/sass/app.scss', lang: 'scss' }],

	plugins: ['~plugins/i18n', '~plugins/vform', '~plugins/axios', '~plugins/fontawesome', '~plugins/nuxt-client-init', '~utils/notif.js', '~utils/navigations'],

	modules: [
		'@nuxtjs/router',
		[
			'@nuxtjs/vuetify',
			{
				theme: {
					themes: {
						light: {
							secondary: '#F6F9FB',
							primary: '#0065BD',
							success: '#66BB6A',
							danger: '#FF5252',
							warning: '#FFA726',
							info: '#1E88E5',
						},
					},
				},
			},
		],
	],

	build: {
		extractCSS: true,
	},

	hooks: {
		generate: {
			done(generator) {
				// Copy dist files to public/_nuxt
				if (generator.nuxt.options.dev === false && generator.nuxt.options.mode === 'spa') {
					const publicDir = join(generator.nuxt.options.rootDir, 'public', '/');
					removeSync(publicDir);
					copySync(join(generator.nuxt.options.generate.dir, '/'), publicDir);
					copySync(join(generator.nuxt.options.generate.dir, '200.html'), join(publicDir, 'index.html'));
					removeSync(generator.nuxt.options.generate.dir);
				}
			},
		},
	},
};
